### elevator-test

Ratings:
• It works correctly and does not contain errors
• The code is easy to read and is well commented

Considerations:
The building has 4 floors (3 + ground floor) and 3 elevators.


## Part 1

Develop a simulator that generates a report of elevators use throughout the day, the building has 4 floors (3 + ground floor) and 3 elevators. Implement the simulator using an object - oriented programming paradigm. The configuration (number of floors, elevators and sequences) must be able to change.

We will assume:
• Each complete movement (origin → call, call → destination) takes much less than one minute.
• Elevators behave in a similar way as normal elevators do (ex: you wouldn't have just one elevator doing all of the trips while the others are unused).

If you need to make any assumption, please state it in your response.


## Part 2

Develop a web page so the simulated results can be analyzed in an easy way. Integrate the simulator in a PHP framework (symfony new part_2 --full, using symfony binary) that will generate a web page with the same results as in Part 1.

Install Symfony last stable version full:
symfony new part_2 --full

Set the dummy sqlite for Doctrine because we are not using a database:
DATABASE_URL="sqlite:///%kernel.project_dir%/var/data.db"

Add .htaccess to public folder for development purposes.

Create WebController and define routes, views and create templates to render its views.

php bin/console cache:clear --> Clear cache
php bin/console debug:router --> Show defined routes
