<?php
// show all errors
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

// require dependencies
require_once("elevatorsClass.php");

// create class instance
$floors_num = 4;
$elevators_num = 3;

$building = new BuildingElevators($floors_num, $elevators_num);

// print results
echo '<h2>Part 1 - Building Elevators Simulator</h2>';
echo '<p>The application must process provided sequences and generate a <strong>report that shows for each minute from 09:00h to 20:00h</strong>:
<ul><li>The floor on which each elevator is located at that time.</li>
<li>The total number of floors traveled by each elevator up to that point.</li></ul></p>';
echo '<h3>Results:</h3>';

// generate results
$results = $building->reportResults();

/*
// print totals to check results
echo '<pre>';
var_dump($building->elevators_info);
echo '</pre>';
*/

// results
if (empty($results)) {
    // no results
    echo '<strong>No results</strong>';

} else {
    // table
    echo '<table style="width:100%;max-width:900px;border:1px solid #999;border-collapse:collapse;">'
    .'<thead>'
    .'<tr style="border-bottom:1px solid #999;">'
    .'<th style="text-align:center;padding:5px;">Minute</th>';

    // elevators column titles
    for ($e=1; $e<=$elevators_num; $e++) {
        echo '<th style="text-align:center;padding:5px;">E'.$e.' Floor</th>';
        echo '<th style="text-align:center;padding:5px;">E'.$e.' Traveled Floors</th>';
    }

    echo '</tr>'
    .'</thead>'
    .'<tbody>';

    // rows
    $i = 0;
    foreach ($results as $result) {

        // odd/even row background-color
        $i++;
        $bg_color = ($i%2 == 0 ? '#f5f5f5' : '#fff' );

        echo '<tr style="background-color:'.$bg_color.';">'
        .'<td style="text-align:center;padding:5px;">'.$result['hour'].'</td>';

        // elevators row values
        for ($e=1; $e<=$elevators_num; $e++) {
            echo '<td style="text-align:center;padding:5px;">'.$result['el_'.$e.'_actual'].'</td>';
            echo '<td style="text-align:center;padding:5px;">'.$result['el_'.$e.'_traveled'].'</td>';
        }

        echo '</tr>';
    }

    echo '</tbody>'
    .'</table>';
}
?>
