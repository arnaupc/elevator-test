<?php

// require dependencies
require_once("utilsClass.php");
require_once("generatorClass.php");

/**
 * BuildingElevators
 *
 * @version    1.0
 * @author     Arnau Pujol Cabarrocas <arnaupc@hotmail.com>
 */
class BuildingElevators {

    // define public variables with default values
    public $floors_num = 4;
    public $elevators_num = 3;
    protected $generator = null;


    /**
     * Constructor
     */
    public function __construct($floors_num, $elevators_num) {

        // update construct values if not empty
        if (!empty($floors_num)) $this->floors_num = $floors_num;
        if (!empty($elevators_num)) $this->elevators_num = $elevators_num;

        // init generator
        $this->generator = new dataGenerator($this->floors_num, $this->elevators_num);
    }


    /**
     * function to get elevators movements data
     *
     * @return array
     */
    public function getMovements() {

        /*
        Consideracions:
        Els moviments es generen a partir d'un script segons unes seqüències donades i no es recullen des d'una base de dades.
        En una base de dades tindriem almenys dos taules, una pels ascenssors i una altra amb els moviments relacionades per la id de l'ascensor.
        */

        // get generated data
        $generator = $this->generator;
        $movements = $generator->getMovements();

        // return
        return $movements;
    }


    /**
     * function to get elevators data from movements
     *
     * @return array
     */
    protected function parseMovements($movements) {
        
        // get movements
        if (empty($movements)) {
            return false;
        }

        // init vars
        $data = array();
        $elevators_num = $this->elevators_num;

        // init aux totals
        $aux_totals = array();
        for ( $i=1; $i<=$elevators_num; $i++) {
            $aux_totals[$i] = 0;
        }

        // loop movements
        foreach ($movements as $mov) {

            // init values
            $minute = $mov->minute;
            if (!isset($data[$minute])) $data[$minute] = array();

            $elevator_id = $mov->elevator_id;
            if (!isset($data[$minute][$elevator_id])) $data[$minute][$elevator_id] = array();

            // update totals
            $aux_totals[$elevator_id] = $aux_totals[$elevator_id] + $mov->floors_movement;

            // set values
            $data[$minute][$elevator_id]['actual_floor'] = $mov->floor_dest;
            $data[$minute][$elevator_id]['floors_traveled'] = $aux_totals[$elevator_id];
        }

        return $data;
    }


    /**
     * report results
     *
     * @return array
     */
    public function reportResults() {

        // init vars
        $results = array();
        $elevators_num = $this->elevators_num;

        // init values array: save last data for elevators
        $values = array();
        for ($e=1; $e<=$elevators_num; $e++) {
            $values[$e] = array(
                'actual_floor' => 0,
                'floors_traveled' => 0
            );
        }

        // get elevators data
        $movements = $this->getMovements();
        $data = $this->parseMovements($movements);

        $report_ini = Utils::hoursToMinutes('09:00');
        $report_end = Utils::hoursToMinutes('20:00');
        for ($m = $report_ini; $m <= $report_end; $m++) {

            // check if exists new values for this minute
            for ($e=1; $e<=$elevators_num; $e++) {
                if (isset($data[$m][$e])) {
                    // update values
                    $values[$e] = array(
                        'actual_floor' => $data[$m][$e]['actual_floor'],
                        'floors_traveled' => $data[$m][$e]['floors_traveled'],
                    );
                }
            }

            // add new result
            $result = array(
                'minute' => $m,
                'hour' => Utils::minutesToHours($m),
            );

            for ($e = 1; $e <= $elevators_num; $e++) {
                $result['el_'.$e.'_actual'] = $values[$e]['actual_floor'];
                $result['el_'.$e.'_traveled'] = $values[$e]['floors_traveled'];
            }

            $results[] = $result;
        }

        return $results;
    }
}
?>
