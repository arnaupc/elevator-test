<?php
// src/Lib/utilsClass.php
namespace App\Lib;

/**
 * Utils
 *
 * @version    1.0
 * @author     Arnau Pujol Cabarrocas <arnaupc@hotmail.com>
 */
class Utils {


    /**
     * function to get day minutes from hour string
     *
     * @return int
     */
    public static function hoursToMinutes($time) {

        // string time is format hh:mm
        $parts = explode(':', $time);
        // calc minutes
        $minutes = 60 * (int)$parts[0] + (int)$parts[1];
        // return result
        return $minutes;
    }


    /**
     * function to get hour string from day minutes
     *
     * @return string
     */
    public static function minutesToHours($minutes) {

        // calc hour and minutes
        $hour = (int)($minutes / 60);
        $minutes = $minutes % 60;

        $string = ($hour<10 ? '0':'').$hour.':'.($minutes<10 ? '0':'').$minutes;
        return $string;
    }
}
?>
