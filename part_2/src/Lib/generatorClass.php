<?php
// src/Lib/generatorClass.php
namespace App\Lib;

// require dependencies
use App\Lib\Utils;

/**
 * Generator
 *
 * @version    1.0
 * @author     Arnau Pujol Cabarrocas <arnaupc@hotmail.com>
 */
class dataGenerator {

    // define public variables with default values
    public $floors_num = 4;
    public $elevators_num = 3;
    public $movements = null;
    public $elevators_info = null;


    /**
     * Constructor
     */
    public function __construct($floors_num, $elevators_num) {

        // update construct values if not empty
        if (!empty($floors_num)) $this->floors_num = $floors_num;
        if (!empty($elevators_num)) $this->elevators_num = $elevators_num;

        // init elevators info array
        $this->elevators_info = $this->initElevatorsInfo();
        $this->movements = array();
    }


    /**
     * function to init elevators data
     */
    protected function initElevatorsInfo() {

        $elevators_num = $this->elevators_num;
        $info = array();

        for ($i = 1; $i<= $elevators_num; $i++) {
            // for each elevator init values for actual floor and floors traveled
            $info[$i] = (object)[
                'actual_floor' => 0,
                'movements' => 0,
                'floors_traveled' => 0,
            ];
        }

        // return result
        return $info;
    }


    /**
     * function to get an elevator for the movement
     */
    protected function getElevatorForMovement($minute, $floor_call) {

        // get elevators num
        $elevators_num = $this->elevators_num;

        // get elevator to move: random selection
        $elevator_id = random_int(1, $elevators_num);

        // get elevator to move: selection based on availability and distance
        // TODO: implement method

        // return id
        return $elevator_id;
    }


    /**
     * function to add elevator movement to array of movements
     */
    protected function addMovement($minute, $movement) {

        // check movements is correct
        if (!is_array($movement) && count($movement)!=2) return false;

        // assign variables as in array
        list($floor_call, $floor_dest) = $movement;

        // get elevator to move by minute and floor called
        $elevator_id = $this->getElevatorForMovement($minute, $floor_call);

        // get elevator data
        $elevators_info = $this->elevators_info[$elevator_id];

        // get actual position of elevator
        $actual_floor = $elevators_info->actual_floor;
        $num_movements = count($this->movements);

        // calcule floors traveled
        $floors_movement = abs($floor_call - $actual_floor) + abs($floor_dest - $floor_call);

        // new object movement
        $mov = (object) [
            'num' => $num_movements + 1,
            'minute' => $minute,
            'elevator_id' => $elevator_id,
            'floor_init' => $actual_floor,
            'floor_call' => $floor_call,
            'floor_dest' => $floor_dest,
            'floors_movement' => $floors_movement,
        ];

        // add object to array
        $this->movements[] = $mov;


        /**
         * Update totals
         */

        // new elevator values
        $updated_data = (object)[
            'actual_floor' => $floor_dest,
            'movements' => $elevators_info->movements + 1,
            'floors_traveled' => $elevators_info->floors_traveled + $floors_movement,
        ];
        // update elevator data
        $this->elevators_info[$elevator_id] = $updated_data;

        // return
        return true;
    }


    /**
     * function to generate elevators movements by defined sequences
     */
    protected function generateMovements() {

        // check values
        $floors_num = $this->floors_num;
        $elevators_num = $this->elevators_num;

        if (empty($floors_num) || empty($elevators_num)) {
            return false;
        }

        // init movement, array of objects
        $movements = array();

        /*
        Consideracions:
        - L'ascensor que s'utilitzarà en cada viatge s'escollirà de forma aleatòria (podríem fer-ho segons disponibilitat i distància fins al pis on es demani)
        - Considerem els viatges independents i que els usuaris no comparteixen ascensor (podríem considerar-ho en els viatges de pujada amb diferents parades)
        - Recorrerem els minuts del dia per simular els moviments segons les seqüències donades, considerem els intervals de la forma (inici,fi] per mantenir els valors inicials en 00:00 i quan es diu cada n minuts considerem $minut % n == 0
        */

        for ($m = 0; $m < 1440; $m++) {

            // 1. Every 5 minutes from 09:00h to 11:00h users call the elevator from the ground floor to go to floor 2
            $init_1 = Utils::hoursToMinutes('09:00');
            $end_1 = Utils::hoursToMinutes('11:00');
            if ( $m > $init_1 && $m <= $end_1 && $m % 5 == 0 ) {
                // if condition is met add movements
                // movement defined as array [floor_call,floor_dest]
                $this->addMovement($m, [0,2]);
            }

            // 2. Every 5 minutes from 09:00h to 11:00h users call the elevator from the ground floor to go to floor 3
            $init_2 = Utils::hoursToMinutes('09:00');
            $end_2 = Utils::hoursToMinutes('11:00');
            if ( $m > $init_2 && $m < $end_2 && $m % 5 == 0 ) {
                // if condition is met add movements
                $this->addMovement($m, [0,3]);
            }

            // 3. Every 10 minutes from 09:00h to 10:00h users call the elevator from the ground floor to the first floor
            $init_3 = Utils::hoursToMinutes('09:00');
            $end_3 = Utils::hoursToMinutes('10:00');
            if ( $m > $init_3 && $m <= $end_3 && $m % 10 == 0 ) {
                // if condition is met add movements
                $this->addMovement($m, [0,1]);
            }

            // 4. Every 20 minutes from 11:00h to 18:20h users call the elevator from the ground floor to go to floors 1, 2 and 3
            $init_4 = Utils::hoursToMinutes('11:00');
            $end_4 = Utils::hoursToMinutes('18:20');

            if ( $m > $init_4 && $m <= $end_4 && $m % 20 == 0 ) {
                // if condition is met add movements
                $this->addMovement($m, [0,1]);
                $this->addMovement($m, [0,2]);
                $this->addMovement($m, [0,3]);
            }

            // 5. Every 4 minutes from 2:00 p.m. to 3:00 p.m. users call the elevator from floors 1, 2 and 3 to go to the ground floor
            $init_5 = Utils::hoursToMinutes('14:00');
            $end_5 = Utils::hoursToMinutes('15:00');
            if ( $m > $init_5 && $m <= $end_5 && $m % 4 == 0 ) {
                // if condition is met add movements
                $this->addMovement($m, [1,0]);
                $this->addMovement($m, [2,0]);
                $this->addMovement($m, [3,0]);
            }

            // 6. Every 7 minutes from 3:00 p.m. to 4:00 p.m. users call the elevator from floors 2 and 3 to go to the ground floor
            $init_6 = Utils::hoursToMinutes('15:00');
            $end_6 = Utils::hoursToMinutes('16:00');
            if ( $m > $init_6 && $m <= $end_6 && $m % 7 == 0 ) {
                // if condition is met add movements
                $this->addMovement($m, [2,0]);
                $this->addMovement($m, [3,0]);
            }

            // 7. Every 7 minutes from 3:00 p.m. to 4:00 p.m. users call the elevator from the ground floor to go to floors 1 and 3
            $init_7 = Utils::hoursToMinutes('15:00');
            $end_7 = Utils::hoursToMinutes('16:00');
            if ( $m > $init_7 && $m <= $end_7 && $m % 7 == 0 ) {
                // if condition is met add movements
                $this->addMovement($m, [0,1]);
                $this->addMovement($m, [0,3]);
            }

            // 8. Every 3 minutes from 6:00 p.m. to 8:00 p.m. users call the elevator from floors 1, 2 and 3 to go to the ground floor
            $init_8 = Utils::hoursToMinutes('18:00');
            $end_8 = Utils::hoursToMinutes('20:00');
            if ( $m > $init_8 && $m <= $end_8 && $m % 3 == 0 ) {
                // if condition is met add movements
                $this->addMovement($m, [1,0]);
                $this->addMovement($m, [2,0]);
                $this->addMovement($m, [3,0]);
            }
        }
    }


    /**
     * function to get generator movements
     */
    public function getMovements() {

        // generate movements if not set
        if (empty($this->movements)) {
            $this->generateMovements();
        }

        $movements = $this->movements;

        return $movements;
    }


    /**
     * function to get generator elevators info
     */
    public function getInfo() {
        
        // generate movements if not set
        if (empty($this->movements)) {
            $this->generateMovements();
        }

        $info = $this->elevators_info;

        return $info;
    }
}
?>
