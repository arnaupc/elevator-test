<?php
// src/Controller/WebController.php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

use App\Lib\BuildingElevators;


class WebController extends AbstractController
{


    /**
     * @Route("/", name="results")
     */
    public function results(): Response
    {
        // set building values
        $floors_num = 4;
        $elevators_num = 3;
        // get results
        $building = new BuildingElevators($floors_num, $elevators_num);
        $results = $building->reportResults();

        // render twig template
        return $this->render('results.html.twig', [
            'elevators_num' => $elevators_num,
            'results' => $results,
        ]);
    }



    /**
     * @Route("/info", name="info")
     */
    public function info()
    {
        // render twig template
        return $this->render('info.html.twig', []);
    }
}
