/*
jQuery scrollTopTop
*/
(function($) {

    $.fn.scrollToTop = function(options) {

        var config = {
            "speed" : 800,
            "offsetTop" : 100
        };

        if (options) {
            $.extend(config,options);
        }

        return this.each(function() {

            var $this = $(this);

            $(window).scroll(function() {
                if ($(this).scrollTop() > config.offsetTop) {
                    $this.fadeIn();
                } else {
                    $this.fadeOut();
                }
            });

            $this.click(function(e) {
                e.preventDefault();
                $("body, html").animate({ scrollTop : 0 }, config.speed);
            });
        });
    };
})(jQuery);


/*
On jQuery ready!
*/
$(document).ready(function () {

    'use strict';

    // --------------
    // scrollToTop
    // --------------

	if ($.fn.scrollToTop) {
        $(".to_top").scrollToTop({offsetTop: 280});
    }
});
